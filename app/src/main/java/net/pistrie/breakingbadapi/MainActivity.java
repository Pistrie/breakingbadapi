package net.pistrie.breakingbadapi;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import net.pistrie.breakingbadapi.activities.ActorBigPictureActivity;
import net.pistrie.breakingbadapi.activities.ActorDetailedActivity;
import net.pistrie.breakingbadapi.domain.Actor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity implements ActorAdapter.ItemClickListener,
        ActorAdapter.PhotoClickListener, LoaderManager.LoaderCallbacks<ArrayList<Actor>> {

    private final String TAG = getClass().getSimpleName();

    private static final int BB_API_LOADER = 1;
    private static final String BB_API_LINK_EXTRA = "api link";
    private static final String BB_API_LINK = "https://www.breakingbadapi.com/api/characters";

    private final static String JSON_ACTOR_ID = "char_id";
    private final static String JSON_ACTOR_NAME = "name";
    private final static String JSON_ACTOR_NICKNAME = "nickname";
    private final static String JSON_ACTOR_IMAGE = "img";
    private final static String JSON_ACTOR_STATUS = "status";
    private final static String JSON_ACTOR_ONLYBCS = "appearance";

    private final static int FILTER_NAME = 0;
    private final static int FILTER_STATUS = 1;
    private final static String FILTER_STATUS_ALIVE = "alive";
    private final static String FILTER_STATUS_DEAD = "deceased";
    private final static String FILTER_STATUS_PRESUMED_DEAD = "presumed dead";
    private final static String FILTER_STATUS_NOFILTER = "no filter";

    private ArrayList<Actor> mActorList = new ArrayList<>();
    private RecyclerView mActorRecyclerView;
    private LinearLayoutManager mLinearLayoutManager = null;
    private GridLayoutManager mGridLayoutManager = null;
    private ActorAdapter mActorAdapter;
    private EditText mEditTextFilter;
    private RadioGroup mRadioGroupFilter;

    private SharedPreferences sp;
    private int showOrHideNameFilter;
    private int showOrHideStatusFilter;

    private static final String ACTORLIST_STATE = "actorlist_state";
    private static final String SHOW_OR_HIDDEN_NAME_FILTER_STATE = "namefilter_state";
    private static final String SHOW_OR_HIDDEN_STATUS_FILTER_STATE = "statusfilter_state";

    private int orientation;

    // ik wil de toast maar 1x zien
    int showItemsLoadedToast;
    @Override
    protected void onPause() {
        this.showItemsLoadedToast = 1;
        super.onPause();
    }

    // huidige staat opslaan
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "saving state");

        outState.putParcelableArrayList(ACTORLIST_STATE, mActorList);
        outState.putInt(SHOW_OR_HIDDEN_NAME_FILTER_STATE, showOrHideNameFilter);
        outState.putInt(SHOW_OR_HIDDEN_STATUS_FILTER_STATE, showOrHideStatusFilter);
    }

    // activity aanmaken
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate called");

        mActorRecyclerView = findViewById(R.id.actors_recycler_view);

        if (mLinearLayoutManager == null || mGridLayoutManager == null) {
            mLinearLayoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mGridLayoutManager
                    = new GridLayoutManager(this, 2);
        }

        // op basis van de orientatie van het apparaat krijg je een LinearLayout of een GridLayout
        orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            mActorRecyclerView.setLayoutManager(mLinearLayoutManager);
        } else {
            mActorRecyclerView.setLayoutManager(mGridLayoutManager);
        }

        sp = getSharedPreferences("MyUserPrefs", Context.MODE_PRIVATE);

        // state herstellen als er een saved state is
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList(ACTORLIST_STATE) != null) {
            Log.d(TAG, "restoring mActorList");
            mActorList = savedInstanceState.getParcelableArrayList(ACTORLIST_STATE);
            if (savedInstanceState.getInt(SHOW_OR_HIDDEN_NAME_FILTER_STATE) == 1) {
                showOrHideNameFilter = 1;
            }
            if (savedInstanceState.getInt(SHOW_OR_HIDDEN_STATUS_FILTER_STATE) == 1) {
                showOrHideStatusFilter = 1;
            }
        } else {
            Log.d(TAG, "calling makeApiRequest");
            makeApiRequest();
        }

        mEditTextFilter = findViewById(R.id.actors_filter);
        mRadioGroupFilter = findViewById(R.id.actors_status_filter);

        // filter toggle
        if (showOrHideNameFilter == 1) {
            mEditTextFilter.setVisibility(View.VISIBLE);
        } else {
            mEditTextFilter.setVisibility(View.GONE);
        }
        if (showOrHideStatusFilter == 1) {
            mRadioGroupFilter.setVisibility(View.VISIBLE);
        } else {
            mRadioGroupFilter.setVisibility(View.GONE);
        }

        // Name filter listener
        mEditTextFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(FILTER_NAME, s.toString());
            }
        });

        mActorAdapter = new ActorAdapter(this, mActorList);
        mActorAdapter.addItemClickListener(this);
        mActorAdapter.addPhotoClickListener(this);
        mActorRecyclerView.setAdapter(mActorAdapter);

        // divider
        DividerItemDecoration dividerItemDecoration
                = new DividerItemDecoration(mActorRecyclerView.getContext(),
                mLinearLayoutManager.getOrientation());
        mActorRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    // starten van de Loader
    public void makeApiRequest() {
        Log.d(TAG, "makeApiRequest called");

        Bundle bbApiBundle = new Bundle();
        bbApiBundle.putString(BB_API_LINK_EXTRA, BB_API_LINK);

        // getSupportManager() is niet beschikbaar in MainMenuAPITask
        // geen andere keuze dan alle api requests hier doen als ik de superieure LoaderManager wil gebruiken
        // zoals uitgelegd in de udacity cursus lesson 5
        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<ArrayList<Actor>> apiLoader = loaderManager.getLoader(BB_API_LOADER);

        // https://stackoverflow.com/a/10537392
        if (apiLoader == null) {
            loaderManager.initLoader(BB_API_LOADER, bbApiBundle, this).forceLoad();
        } else {
            loaderManager.restartLoader(BB_API_LOADER, bbApiBundle, this).forceLoad();
        }
    }

    // menu aanmaken
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // name filter
    private void filter(int filterType, String text) {
        ArrayList<Actor> filteredList = new ArrayList<>();

        // Log weggelaten wegens logspam

        if (filterType == FILTER_NAME) {
            for (Actor actor : mActorList) {
                if (actor.getName().toLowerCase().contains(text.toLowerCase()) ||
                        actor.getNickname().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(actor);
                }
            }
        } else if (filterType == FILTER_STATUS && !text.equals(FILTER_STATUS_NOFILTER)) {
            for (Actor actor : mActorList) {
                if (actor.getStatus().toLowerCase().matches(text.toLowerCase())) {
                    filteredList.add(actor);
                }
            }
        } else if (filterType == FILTER_STATUS && text.equals(FILTER_STATUS_NOFILTER)) {
            filteredList.addAll(mActorList);
        }

        mActorAdapter.filterList(filteredList);
    }

    // listener voor de recycler view
    public void onActorAvailable(List<Actor> actors) {
        Log.d(TAG, "onActorAvailable -> We have " + actors.size() + " items");

        this.mActorList.clear();
        this.mActorList.addAll(actors);
        this.mActorAdapter.notifyDataSetChanged();
    }

    // item click listener voor de recycler view
    @Override
    public void onItemClick(int position, String actorName) {
        String jsonIndex = "https://www.breakingbadapi.com/api/characters?name=" + actorName;
        Log.d(TAG, "onItemClick -> url = " + jsonIndex);

        Intent intent = new Intent(this, ActorDetailedActivity.class);
        Bundle b = new Bundle();
        b.putString("url", jsonIndex);
        intent.putExtras(b);
        startActivity(intent);
    }

    // photo click listener voor de recycler view
    @Override
    public void onPhotoClick(int position, String actorName) {
        String jsonIndex = "https://www.breakingbadapi.com/api/characters?name=" + actorName;
        Log.d(TAG, "onItemClick -> url = " + jsonIndex);

        Intent intent = new Intent(this, ActorBigPictureActivity.class);
        Bundle b = new Bundle();
        b.putString("url", jsonIndex);
        intent.putExtras(b);
        startActivity(intent);
    }

    // loader manager die api requests maakt
    @NonNull
    @Override
    public Loader<ArrayList<Actor>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.d(TAG, "onCreateLoader called");

        ArrayList<Actor> mActorList = new ArrayList<>();

        return new AsyncTaskLoader<ArrayList<Actor>>(this) {

            // als mActorList leeg is wordt er een api request gedaan
            // anders wordt de list gewoon ingeladen
            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                Log.d(TAG, "onStartLoading Called");
                if (!mActorList.isEmpty()) {
                    Log.d(TAG, "onStartLoading list size -> " + mActorList.size());
                    deliverResult(mActorList);
                } else {
                    Log.d(TAG, "onStartLoading -> list is empty");
                    forceLoad();
                }
            }

            // de background load
            @Nullable
            @Override
            public ArrayList<Actor> loadInBackground() {
                Log.d(TAG, "loadInBackground called");
                String apiLink = args.getString(BB_API_LINK_EXTRA);
                Log.d(TAG, "apiLink -> " + apiLink);
                if (apiLink == null || TextUtils.isEmpty(apiLink)) {
                    return null;
                }
                URL url = null;
                HttpURLConnection urlConnection = null;

                try {
                    url = new URL(apiLink);

                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    Scanner scanner = new Scanner(in);
                    scanner.useDelimiter("\\A");

                    boolean hasInput = scanner.hasNext();
                    if (hasInput) {
                        String response = scanner.next();
                        Log.d(TAG, "response -> " + response);

                        return convertJsonToArrayList(response);
                    } else {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                } finally {
                    if (null != urlConnection) {
                        urlConnection.disconnect();
                    }
                }
            }

            // teruggeven van een gevulde list
            @Override
            public void deliverResult(@Nullable ArrayList<Actor> data) {
                Log.d(TAG, "data size -> " + data.size());
                ArrayList<Actor> mActorListCopy = new ArrayList<>(data);

                mActorList.clear();
                mActorList.addAll(mActorListCopy);
                Log.d(TAG, "deliverResult -> mActorList contains " + mActorList.size() + " items after addAll");
                super.deliverResult(mActorList);
            }
        };
    }

    // hulpmethode
    private ArrayList<Actor> convertJsonToArrayList(String response) {
        ArrayList<Actor> resultList = new ArrayList<>();

        // omzetten van String naar ArrayList
        try {
            JSONArray actors = new JSONArray(response);
            for (int i = 0; i < actors.length(); i++) {
                JSONObject actor = actors.getJSONObject(i);

                String id = actor.getString(JSON_ACTOR_ID);
                String name = actor.getString(JSON_ACTOR_NAME);
                String nickname = actor.getString(JSON_ACTOR_NICKNAME);
                String status = actor.getString(JSON_ACTOR_STATUS);
                String image = actor.getString(JSON_ACTOR_IMAGE);
                String onlyBCS = actor.getString(JSON_ACTOR_ONLYBCS);

                // we dont want actors who only play in better call saul
                if (!onlyBCS.equals("[]")) {
                    resultList.add(new Actor(id, name, nickname, status, image));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "convertJsonToArrayList -> returning " + resultList.size() + " items");

        return resultList;
    }

    // teruggeven van data
    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<Actor>> loader, ArrayList<Actor> data) {
        Log.d(TAG, "onLoadFinished called");
        Log.d(TAG, "data -> " + data.size());
        Log.d(TAG, "mActorList -> " + mActorList.size());

        // dubbelcheck
        if (null != mActorList) {
            onActorAvailable(data);

            Log.d(TAG, "showItemsLoadedToast -> " + showItemsLoadedToast);
            if (this.showItemsLoadedToast < 1) {
                Toast.makeText(this, getResources().getQuantityString(R.plurals.on_actor_available_toast_loaded, data.size(), data.size()), Toast.LENGTH_SHORT).show();
            }
        } else {
            onActorAvailable(mActorList);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<Actor>> loader) {
        // nothing
    }

    // toggle voor de options items
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        Log.d(TAG, "onOptionsItemSelected item clicked -> " + itemThatWasClickedId);

        if (itemThatWasClickedId == R.id.action_search) {
            Log.d(TAG, "showOrHideNameFilter -> " + showOrHideNameFilter);
            if (showOrHideNameFilter == 1) {
                mEditTextFilter.setVisibility(View.GONE);
                showOrHideNameFilter = 0;
            } else {
                mEditTextFilter.setVisibility(View.VISIBLE);
                showOrHideNameFilter = 1;
                return true;
            }
        } else if (itemThatWasClickedId == R.id.action_status_filter) {
            Log.d(TAG, "showOrHideStatusFilter -> " + showOrHideStatusFilter);
            if (showOrHideStatusFilter == 1) {
                mRadioGroupFilter.setVisibility(View.GONE);
                showOrHideStatusFilter = 0;
            } else {
                mRadioGroupFilter.setVisibility(View.VISIBLE);
                showOrHideStatusFilter = 1;
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    // radio buttons click listener
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        SharedPreferences.Editor editor = sp.edit();

        switch (view.getId()) {
            case R.id.actors_status_filter_alive:
                if (checked) {
                    filter(FILTER_STATUS, FILTER_STATUS_ALIVE);
                    editor.putString("filterType", FILTER_STATUS_ALIVE);
                }
                break;
            case R.id.actors_status_filter_dead:
                if (checked) {
                    filter(FILTER_STATUS, FILTER_STATUS_DEAD);
                    editor.putString("filterType", FILTER_STATUS_DEAD);
                }
                break;
            case R.id.actors_status_filter_presumed_dead:
                if (checked) {
                    filter(FILTER_STATUS, FILTER_STATUS_PRESUMED_DEAD);
                    editor.putString("filterType", FILTER_STATUS_PRESUMED_DEAD);
                }
                break;
            case R.id.actors_status_filter_nofilter:
                if (checked) {
                    filter(FILTER_STATUS, FILTER_STATUS_NOFILTER);
                    editor.putString("filterType", FILTER_STATUS_NOFILTER);
                }
                break;
        }
        Log.d(TAG, "onRadioButtonClicked clicked on button -> " + view.getId());
        editor.apply();
        Toast.makeText(MainActivity.this, "Filter saved", Toast.LENGTH_SHORT).show();
    }
}