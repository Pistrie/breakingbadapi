package net.pistrie.breakingbadapi.domain;

import java.util.ArrayList;
import java.util.StringJoiner;

public class ActorDetailed {

    // attributes
    private final String id;
    private final String name;
    private final String nickname;
    private final String imageUrl;
    private final String status;
    private final String birthday;
    private final String[] occupations;
    private final int[] seasonAppearances;
    private final ArrayList<String> quotes;

    public ActorDetailed(String id, String name, String nickname, String imageUrl, String status, String birthday, String[] occupations, int[] seasonAppearances, ArrayList<String> quotes) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.imageUrl = imageUrl;
        this.status = status;
        this.birthday = birthday;
        this.occupations = occupations;
        this.seasonAppearances = seasonAppearances;
        this.quotes = quotes;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getStatus() {
        return status;
    }

    public String getBirthday() {
        return birthday;
    }

    public String[] getOccupations() {
        int counter = 0;
        StringJoiner joiner = new StringJoiner(", ");
        for (String occupation : occupations) {
            joiner.add(occupation);
            counter++;
        }
        return new String[]{String.valueOf(counter), "\n" + joiner.toString()};
    }

    public String[] getSeasonAppearances() {
        int counter = 0;
        StringJoiner joiner = new StringJoiner(", ");
        for (int appearance : seasonAppearances) {
            joiner.add(appearance + "");
            counter++;
        }
        return new String[]{String.valueOf(counter), "\n" + joiner.toString()};
    }

    public String[] getQuotes() {
        int counter = 0;
        StringJoiner joiner = new StringJoiner("\n");
        for (String quote : quotes) {
            joiner.add("\"" + quote + "\"");
            counter++;
        }
        return new String[]{String.valueOf(counter), "\n" + joiner.toString()};
    }
}
