package net.pistrie.breakingbadapi.domain;

public class ActorBigPicture {

    private final String imageUrl;

    public ActorBigPicture(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
