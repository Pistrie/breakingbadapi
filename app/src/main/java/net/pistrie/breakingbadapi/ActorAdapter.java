package net.pistrie.breakingbadapi;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import net.pistrie.breakingbadapi.domain.Actor;

import java.util.ArrayList;
import java.util.List;

public class ActorAdapter extends RecyclerView.Adapter<ActorAdapter.ActorViewHolder> {

    private final String TAG = getClass().getSimpleName();

    Context context;
    private List<Actor> mActorList;
    private ItemClickListener mItemClickListener;
    private PhotoClickListener mPhotoClickListener;
    Resources res;

    // constructor
    public ActorAdapter(Context context, List<Actor> mActorList) {
        Log.d(TAG, "Constructor aangeroepen");
        this.context = context;
        this.mActorList = mActorList;
    }

    // aanmaken van list items
    @NonNull
    @Override
    public ActorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        Log.d(TAG, "onCreate called");
        // Logspam

        res = this.context.getResources();

        int layoutIdForListItem = R.layout.actor_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        return new ActorViewHolder(view);
    }

    // vullen van list items
    @Override
    public void onBindViewHolder(@NonNull ActorViewHolder holder, int position) {
        Actor actor = this.mActorList.get(position);

        // Log weggelaten vanwege logspam

        holder.mActorName.setText(actor.getName());
        holder.mActorNickname.setText(res.getString(R.string.on_bind_view_holder_nickname, actor.getNickname()));
        holder.mActorStatus.setText(res.getString(R.string.on_bind_view_holder_status, actor.getStatus()));
        Picasso
                .get()
                .load(actor.getImageUrl())
                .resize(300, 0)
                .placeholder(R.drawable.progress_animation)
                .into(holder.mActorImageView);
        holder.mLinearLayout.setOnClickListener(v -> {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(position, actor.getName());
            }
        });
        holder.mActorImageView.setOnClickListener(v -> {
            if (mItemClickListener != null) {
                mPhotoClickListener.onPhotoClick(position, actor.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        // Log weggelaten vanwege logspam
        return this.mActorList.size();
    }

    // filteren van de mActorList
    public void filterList(ArrayList<Actor> filteredList) {
        mActorList = filteredList;
        notifyDataSetChanged();
    }

    public class ActorViewHolder extends RecyclerView.ViewHolder {

        private final String TAG = getClass().getSimpleName();

        public LinearLayout mLinearLayout;
        public TextView mActorName;
        public TextView mActorNickname;
        public TextView mActorStatus;
        public ImageView mActorImageView;

        public ActorViewHolder(@NonNull View itemView) {
            super(itemView);

//            Log.d(TAG, "Constructor called");
            // Logspam

            mLinearLayout = itemView.findViewById(R.id.actor_list_item_linear_details_layout);
            mActorName = itemView.findViewById(R.id.actor_list_item_name);
            mActorNickname = itemView.findViewById(R.id.actor_list_item_nickname);
            mActorStatus = itemView.findViewById(R.id.actor_list_item_status);
            mActorImageView = itemView.findViewById(R.id.actor_list_item_image);
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position, String actorName);
    }

    public interface PhotoClickListener {
        void onPhotoClick(int position, String actorName);
    }

    public void addItemClickListener(ItemClickListener listener) {
        Log.d(TAG, "Adding ItemClickListener");
        this.mItemClickListener = listener;
    }

    public void addPhotoClickListener(PhotoClickListener listener) {
        Log.d(TAG, "Adding PhotoClickListener");
        this.mPhotoClickListener = listener;
    }
}
